package com.swoop.example.ws;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.swoop.example.service.MathService;

@RestController
@RequestMapping(value = "math")
public class MathController {

	private final MathService service;

	public MathController(MathService service) {
		this.service = service;
	}
	
	@GetMapping(value = "/add", produces = "application/json")
	public RestMathReturn addParam(
			@RequestParam(name = "n1", required = false, defaultValue = "0") Long n1, 
			@RequestParam(name = "n2", required = false, defaultValue = "0") Long n2) {
		return service.add(n1, n2);
	}
	
	@PostMapping(value = "/add", produces = "application/json", consumes = "application/json")
	public RestMathReturn addAttribute(@RequestBody(required = true) RequestBodyObject body) {
		return service.add(body.getN1(), body.getN2());
	}

}
