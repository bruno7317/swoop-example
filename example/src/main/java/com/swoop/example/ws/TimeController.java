package com.swoop.example.ws;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.swoop.example.service.TimeService;

@RestController
@RequestMapping(value = "time")
public class TimeController {
	
	private final TimeService service;
	
	public TimeController(TimeService service) {
		this.service = service;
	}

	@GetMapping(value = "/now", produces = "application/json")
	public RestTimeReturn now() {
		return service.now();
	}

}
