package com.swoop.example.ws;

public class RestMathReturn {

	private long result;

	public long getResult() {
		return result;
	}

	public void setResult(long result) {
		this.result = result;
	}

}
