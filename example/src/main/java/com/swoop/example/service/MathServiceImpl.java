package com.swoop.example.service;

import org.springframework.stereotype.Service;

import com.swoop.example.ws.RestMathReturn;

@Service
public class MathServiceImpl implements MathService {

	@Override
	public RestMathReturn add(long n1, long n2) {
		RestMathReturn result = new RestMathReturn();
		result.setResult(Math.addExact(n1, n2));
		return result;
	}

}
