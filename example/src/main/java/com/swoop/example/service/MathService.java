package com.swoop.example.service;

import com.swoop.example.ws.RestMathReturn;

public interface MathService {

	public RestMathReturn add(long n1, long n2);
}
