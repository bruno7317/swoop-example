package com.swoop.example.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.swoop.example.ws.RestTimeResponse;
import com.swoop.example.ws.RestTimeReturn;

@Service
public class TimeServiceImpl implements TimeService {
	
	private final String MST = "MST";

	private final String URL = "http://api.timezonedb.com/v2.1/get-time-zone?key=X1UAJO94CIXV&format=json&by=zone&zone=Europe/Oslo";
	
	private final RestTemplate restTemplate;
	
	public TimeServiceImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public RestTimeReturn now() {
        RestTimeResponse response = restTemplate.getForObject(URL, RestTimeResponse.class);
        
        DateFormat sdfCet = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdfCet.setTimeZone(TimeZone.getTimeZone(response.getAbbreviation()));
        
        DateFormat sdfMst = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdfMst.setTimeZone(TimeZone.getTimeZone(MST));
        
        RestTimeReturn timeReturn = new RestTimeReturn();
        try {
        	Date osloDate = sdfCet.parse(response.getFormatted());
        	String calgaryDate = sdfMst.format(osloDate);
        	
			timeReturn.setCurrentTime("Time in Oslo: " + response.getFormatted() + ". Time in Calgary: " + calgaryDate + ".");
		} catch (ParseException e) {
			e.printStackTrace();
		}
        
		return timeReturn;
	}

}
