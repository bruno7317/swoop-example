package com.swoop.example.service;

import com.swoop.example.ws.RestTimeReturn;

public interface TimeService {
	
	public RestTimeReturn now();

}
