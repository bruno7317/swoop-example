To run the service, download the jar file and run it locally.

Docker, AWS and Gradlew weren't used because of lacking experience and knowledge.

A set of Postman integration tests was attached to the email.

The API key was kept on the JAR file otherwise it wouldn't be able to access the external web service. 
In real world applications it would be kept in the server files and retrieved using Java Properties.

Caching in this case would be performed by using the @EnableCaching on class Config and @Cacheable on return methods.
It can be useful to reduce server stress but might return outdated information.

---